import {request} from './request';

export function getHomeMultidata() {
  return request({
    url: '/home/multidata.php'
  })
}


export function getHomeGoodsData(type,page,limit=10) {
  return request({
    url: '/home/homegoods.php',
    method: 'post',
    data: {
      type,
      page,
      limit
    
    }
  })
}
