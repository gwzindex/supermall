import {request} from './request';
export function getDetail(id) {
  return request({
    url: '/detail/detail.php'
  })
}

export class Goods {
  constructor(itemInfo) {
    this.name = itemInfo.name
    this.desc = itemInfo.desc
    this.newPrice = itemInfo.newPrice
    this.oldPrice = itemInfo.oldPrice
    this.bannerlist = itemInfo.bannerlist
  }
}