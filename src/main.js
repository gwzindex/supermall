import Vue from 'vue'
import App from './App.vue'
import router from './router/index'

// import { Button } from 'vant';
// import 'vant/lib/index.css';

// Vue.use(Button);

Vue.prototype.$bus = new Vue()

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
